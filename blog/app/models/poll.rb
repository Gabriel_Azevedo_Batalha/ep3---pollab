class Poll < ApplicationRecord
    has_many :options
    belongs_to :user
    serialize :votes, Array
end
