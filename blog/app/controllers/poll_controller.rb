class PollController < ApplicationController
    def index
        @polls = Poll.all
    end
    def create
        @user = User.find($sessionID)
        Poll.create :title=>params[:title], :text=>params[:text], :created=>false, :textA=>params[:textA], :textB=>params[:textB], :user=>@user, :votes=>["0"], :closed=> false
        redirect_to "/poll"
    end
    def show
        @poll = Poll.find params[:id]
        @poll.save
    end
    def vote
        @option = Option.find params[:id]
        @poll = Poll.find(@option.poll.id)
        if ((@poll.user.id != $sessionID) && @poll.votes.include?($sessionID) == false && @poll.closed == false)
            @option.votes += 1;
        end
        if(@poll.votes.size == 0)
            @poll.votes = ["0"]
        end
        @poll.votes.push($sessionID)
        @option.save
        @poll.save
        redirect_to "/poll/#{@option.poll.id}"
    end
    helper_method :vote
    def createOptions(textA, textB)
        @poll = Poll.find params[:id]
        if (@poll.created == false)
            @option = Option.create :description=>textA, :poll=>@poll, :votes=>0
            @option = Option.create :description=>textB, :poll=>@poll, :votes=>0
            @poll.update_attributes(:created => true)
            @poll.save
            
        end
    end
    helper_method :createOptions
    def close
        @poll = Poll.find params[:id]
        @poll.closed = true
        @poll.title = @poll.title + "[CLOSED]"
        @poll.save
        redirect_to "/poll/#{@poll.id}"
    end
end

