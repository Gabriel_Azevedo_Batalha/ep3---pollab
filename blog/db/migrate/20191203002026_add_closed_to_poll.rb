class AddClosedToPoll < ActiveRecord::Migration[6.0]
  def change
    add_column :polls, :closed, :boolean
  end
end
