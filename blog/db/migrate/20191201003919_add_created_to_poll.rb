class AddCreatedToPoll < ActiveRecord::Migration[6.0]
  def change
    add_column :polls, :created, :boolean
  end
end
