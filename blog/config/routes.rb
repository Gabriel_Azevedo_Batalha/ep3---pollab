Rails.application.routes.draw do
  get '/poll', to: 'poll#index'
  post '/poll', to: 'poll#create'
  get '/poll/:id', to: 'poll#show'
  post '/poll/:id', to: 'poll#close'
  get '/poll/:id/:id', to: 'poll#temp'
  get '/' => 'users#index'
  post 'sessions' => 'sessions#create'
  post '/users' => 'users#create'
end
