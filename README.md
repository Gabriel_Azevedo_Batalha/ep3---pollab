# PolLab
## Participantes:
- Victor Hugo Siqueira Costa / 180149598 / Turma: Carla
- Gabriel Azevedo Batalha/ 180100840 / Turma: Carla

## Resumo do Tema proposto
Um site de votação rápido onde o usuário logado poderá criar uma votação com título, pequena descrição, opções (duas), e encerrar a votação quando desejado. Basta enviar o link para qualquer usuário e ele poderá escolher uma das opções da votação e ver imediatamente o resultado da votação no momento atual, também é possível procurar votações na página inicial. O usuário cadastrado poderá ver depois, em sua conta, a lista das votações que participou. Para usar as funcionalidades do site é necessário estar logado.

## Funcionalidades
- Cadastro e login de usuário que poderá :
    - Criar a votação
    - Encerrar suas votações
    - Votar em votações de terceiros
    - Visualizar os dados da votação na hora
    
## Como executar
1. Clonar o repositório
2. Acessar o diretório
3. Possuir todos os pré-requisitos para rodar o Ruby on Rails
4. Executar  
    `bundle install`  
    `rails migrate`  
    `rails s`  
5. Abrir o navegador em http://localhost:3000/ 

## Usabilidade
- Se cadastrar:
    1. Inserir Nome de usuário, email válido e senha
    2. Clicar em **Submit Query** ao lado
    3. Conta cadastrada, mas, ainda é necessário fazer o login
- Logar:
    1. Inserir email e senha correspondente
    2. Clicar em **Submit Query** ao lado
    3. Logado, agora poderá utilizar as funcionalidades do site
- Criar Votação:
    1. Inserir as informações indicadas 
    2. Clicar em **Create**
    3. A votação será criada e aparecerá em **Your Polls**
- Fechar Votação:
    1. Abrir qualquer votação sua (**Your Polls**)
    2. Clicar em **Close**
    3. A votação será encerrada e receberá a tag **[CLOSED]**
- Votar:
    1. Abrir qualquer votação de terceiro, que você ainda não votou (**Other Polls**)
    2. Clicar em **Vote**
    3. Seu voto será computado e você não poderá mais votar na mesma votação
    4. Agora a votação será visível em **Voted Polls** e não mais **Other Polls**
     
     
    